**Angular 2 | Demo**

for running application:
    
    - create db in your MySql with next props:
            * db-name: angular_demo
            * db-user: angular_demo
            * db-pass: angular_demo
        
    - build pom.xml with maven
    - run springBoot application
    - go to http://localhost:8787