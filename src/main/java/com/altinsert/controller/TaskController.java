package com.altinsert.controller;

import com.altinsert.dao.TaskDao;
import com.altinsert.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping("/task")
public class TaskController {

    @Autowired
    TaskDao taskDao;

    @ResponseBody
    @RequestMapping(method = POST)
    public Task save(@RequestBody Task task) {
        return taskDao.save(task);
    }

    @ResponseBody
    @RequestMapping(method = GET)
    public List<Task> getAll() {
        return taskDao.findAll();
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Task update(@PathVariable long id) {
        Task task = taskDao.findOne(id);
        task.setCompleted(!task.getCompleted());
        return taskDao.save(task);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public void delete(@PathVariable long id) {
        taskDao.delete(id);
    }
}
