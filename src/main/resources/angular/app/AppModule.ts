import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./component/AppComponent/AppComponent";
import { FormsModule } from "@angular/forms";
import { TodoFormComponent } from "./component/TodoFormComponent/TodoFormComponent";
import { TaskListComponent } from "./component/TaskListComponent/TaskListComponent";
import { TaskComponent } from "./component/TaskComponent/TaskComponent";
import { TaskService } from "./service/TaskService";
import { HttpModule } from "@angular/http";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    declarations: [
        AppComponent,
        TodoFormComponent,
        TaskListComponent,
        TaskComponent
    ],
    providers: [TaskService],
    bootstrap: [AppComponent]
})
export class AppModule {

}