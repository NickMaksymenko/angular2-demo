export class Task {
    id: number;

    constructor(
        public title: string,
        public completed: boolean = false
    ){};
}