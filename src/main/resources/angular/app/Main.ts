import 'reflect-metadata';
import 'es6-shim';
import 'zone.js';
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { AppModule } from "./AppModule";

const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);