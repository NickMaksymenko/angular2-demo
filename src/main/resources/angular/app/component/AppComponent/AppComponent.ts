import { Component, ViewEncapsulation } from "@angular/core";

@Component({
    selector: 'app',
    templateUrl: 'AppComponent.html',
    styleUrls: ['AppComponent.css'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    title: string = "Angular 2 | Demo";
}