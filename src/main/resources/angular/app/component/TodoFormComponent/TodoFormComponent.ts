import { Component } from "@angular/core";
import { TaskService } from "../../service/TaskService";

@Component({
    selector: 'todo-form',
    templateUrl: 'TodoFormComponent.html',
    styleUrls: ['TodoFormComponent.css']
})
export class TodoFormComponent {
    newTaskTitle: string = '';

    constructor(private taskService: TaskService) {}

    createTask() {
        this.taskService.createTask(this.newTaskTitle);
    }
}