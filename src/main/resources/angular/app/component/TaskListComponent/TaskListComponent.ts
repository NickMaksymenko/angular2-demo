import { Component, OnInit } from "@angular/core";
import { Task } from "../../shared/Task";
import { TaskService } from "../../service/TaskService";

@Component({
    selector: 'task-list',
    templateUrl: 'TaskListComponent.html',
    styleUrls: ['TaskListComponent.css']
})
export class TaskListComponent implements OnInit {
    taskList: Task[] = [];

    constructor(private taskService: TaskService) {}

    ngOnInit() {
        this.taskService.getTaskList().then(taskList => this.taskList = taskList);
    }

    onDelete(task: Task) {
        this.taskService.deleteTask(task);
    }

    toggleTask(task: Task) {
        this.taskService.toggleTask(task);
    }
}