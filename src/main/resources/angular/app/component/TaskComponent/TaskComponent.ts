import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Task } from "../../shared/Task";

@Component({
    selector: 'task',
    templateUrl: 'TaskComponent.html',
    styleUrls: ['TaskComponent.css']
})
export class TaskComponent {
    @Input() task: Task;
    @Output() onDelete = new EventEmitter();
    @Output() onToggle = new EventEmitter();

    toggleTask() {
        this.onToggle.emit(this.task);
    }

    deleteTask() {
        this.onDelete.emit(this.task);
    }
}