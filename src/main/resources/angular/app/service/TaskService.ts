import { Task } from "../shared/Task";
import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions } from "@angular/http";
import "rxjs/add/operator/toPromise";

const serverHost: string = 'http://localhost:8787/task';

@Injectable()
export class TaskService {

    taskList: Task[];

    constructor(private http: Http) {}

    getTaskList(): Promise<Task[]> {
        return this.http.get(serverHost)
            .toPromise()
            .then(res => res.json())
            .then(taskList => this.taskList = taskList)
            .catch(this.handleError);
    }

    createTask(title: string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });
        let task = new Task(title);

        this.http.post(serverHost, task, options)
            .toPromise()
            .then(res => res.json())
            .then(task => this.taskList.push(task))
            .catch(this.handleError);
    }

    deleteTask(task: Task) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });
        let url = `${serverHost}/${task.id}`;

        this.http.delete(url, options)
            .toPromise()
            .then(res => {
                let index = this.taskList.indexOf(task);

                if (index > -1) {
                    this.taskList.splice(index, 1);
                }
            })
            .catch(this.handleError);
    }

    toggleTask(task: Task) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers });
        let url = `${serverHost}/${task.id}`;

        this.http.put(url, task, options)
            .toPromise()
            .then(res => res.json())
            .then(updatedTask => task.completed = updatedTask.completed)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error(`Error: ${error}`);
        return Promise.reject(error.message || error);
    }
}