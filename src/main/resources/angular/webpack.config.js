const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const publicPath = './';
const jsName = 'bundle.js';
const cssName = 'styles.css';
const isDevelopment = process.env.NODE_ENV === 'dev';
const isProduction = process.env.NODE_ENV === 'production';

const indexPath = './index.html';

const plugins = [
    new ExtractTextPlugin(cssName),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'dev')
        }
    }),
    new HtmlWebpackPlugin({
        template: indexPath
    })
];

if (isProduction) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false,
            drop_console: true
        }
    }));
}

module.exports = {
    plugins,
    entry: './app/Main.ts',
    watch: false,
    devtool: isDevelopment ? 'source-map' : false,
    resolve: { extensions: ['.ts', '.tsx', '.js','.css'] },
    output: {
        path: `${__dirname}/../static`,
        filename: jsName,
        publicPath
    },
    module: {
        rules: [
            {test: /\.ts(x?)$/, exclude: /node_modules/, include: /app/, loaders: ['awesome-typescript-loader', 'angular2-template-loader']},
            {test: /\.js$/, enforce: 'pre', loader: "source-map-loader"},
            {test: /\.html$/, loader: 'raw-loader', exclude: [indexPath]},
            {test: /\.(png|jpg|jpeg|gif|svg)$/, loader: 'url-loader', query: {limit: 25000}},
            {test: /\.css$/, loader: 'css-to-string-loader!css-loader'},
            {test: /\.scss$/, loaders: ['style', 'css', 'postcss', 'sass']},
            {test: /\.(woff2?|ttf|eot|svg)$/, loader: 'url-loader?limit=10000'},
            {test: /bootstrap\/dist\/js\/umd\//, loader: 'imports?jQuery=jquery'}
        ]
    }
};